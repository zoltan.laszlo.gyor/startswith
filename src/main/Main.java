package main;

public class Main {
	public static void main(String[] args) {
		String text1 = "Hey, how are you?";
		System.out.println(text1.startsWith("He"));
		System.out.println(text1.startsWith("e"));
		
		System.out.println(text1.startsWith("y,", 2));
	}
}
